import { useState } from 'react'
import { IconEye, IconEyeOff } from '@tabler/icons-react'

import logo from '../../assets/logo.png'
import { loginSchema } from '../../schema/loginSchema'
import { Form } from '../general/form'
import Input from '../general/input'

type Login = {
	email: string
	password: string
}

function LoginForm() {
	const [showPassword, setShowPassword] = useState(false)

	function onSubmit(data: Login) {
		console.log(data)
	}

	return (
		<div className='xs:border-none w-full rounded-md border border-white bg-white shadow-lg shadow-gray-400'>
			<Form<Login>
				onSubmit={onSubmit}
				validations={loginSchema}
				className='mb-0 space-y-4 rounded-lg p-4 sm:p-6 lg:p-8'>
				{({ register, formState: { errors } }) => (
					<>
						<p className='text-start text-lg font-bold text-blue-1100'>
							Bienvenidos
						</p>
						<div className='h-px w-full bg-gray-300'></div>
						<img src={logo} alt='' className='mx-auto w-32' />

						<Input
							{...register('email')}
							label='Correo electrónico'
							type='email'
							placeholder='Correo electrónico'
							error={errors.email}
						/>
						<div className="relative">
							<Input
								{...register('password')}
								label='Contraseña'
								placeholder='Contraseña'
								error={errors.password}
								type={showPassword ? 'text' : 'password'}
							/>
							<span className='absolute inset-y-0 end-0 w-10 h-10 inline-flex items-center'>
								<button
									type='button'
									className='text-gray-600 hover:text-gray-600'
									onClick={() => setShowPassword(!showPassword)}>
									<p className='sr-only border-none'>
										{showPassword ? 'Ocultar contraseña' : 'Mostrar contraseña'}
									</p>

									{showPassword ? (
										<IconEye className='h-5 text-blue-1000' />
									) : (
										<IconEyeOff className='h-5 text-blue-1000' />
									)}
								</button>
							</span>
						</div>
						<button
							type='submit'
							className='block w-full rounded-lg border-none bg-blue-1000 px-5 py-3 text-sm font-medium text-white hover:bg-blue-1100'>
							Ingresar
						</button>
						<div>
							<p className='text-xs font-bold text-blue-1100'>
								PROTEÍNA ANIMAL S.A DE C.V.
							</p>
							<p className='text-xs'>TODOS LOS DERECHOS RESERVADOS</p>
						</div>
					</>
				)}
			</Form>
		</div>
	)
}

export default LoginForm
