import { DetailedHTMLProps, forwardRef } from 'react'
import { FieldError } from 'react-hook-form'

type InputProps = DetailedHTMLProps<
	React.InputHTMLAttributes<HTMLInputElement>,
	HTMLInputElement
>
interface inputProps extends InputProps {
	label?: string
	placeholder?: string
	name: string
	error: FieldError | undefined
}

const Input = forwardRef<HTMLInputElement, inputProps>((props, ref) => {
	const { label, placeholder, name, error, type, ...rest } = props
	return (
		<div className='relative'>
			<label htmlFor={name} className='sr-only'>
				{label}
			</label>

			<div className='relative'>
				<input
					className={`w-full rounded-md border border-blue-1000 px-3 py-2 text-sm shadow-sm focus:border-blue-1100 focus:outline-none`}
					placeholder={placeholder}
					ref={ref}
					name={name}
					{...rest}
					type={type}
				/>
			</div>
			{error && (
				<p className='mt-2 text-start text-sm font-light text-red-600'>
					{error.message}
				</p>
			)}
		</div>
	)
})

Input.displayName = "Input";

export default Input;
