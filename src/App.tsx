import { BrowserRouter, Route, Routes } from 'react-router-dom'

import Login from './pages/login'

import './App.css'

function App() {
	return (
		<div className='font-goldplay'>
			<BrowserRouter>
				<Routes>
					<Route path='/login' element={<Login />} />
					<Route path='*' element={<h1>No se encontro</h1>} />
				</Routes>
			</BrowserRouter>
		</div>
	)
}

export default App
