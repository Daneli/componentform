# React + TypeScript + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

## Expanding the ESLint configuration

If you are developing a production application, we recommend updating the configuration to enable type aware lint rules:

- Configure the top-level `parserOptions` property like this:

```js
   parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: ['./tsconfig.json', './tsconfig.node.json'],
    tsconfigRootDir: __dirname,
   },
```

- Replace `plugin:@typescript-eslint/recommended` to `plugin:@typescript-eslint/recommended-type-checked` or `plugin:@typescript-eslint/strict-type-checked`
- Optionally add `plugin:@typescript-eslint/stylistic-type-checked`
- Install [eslint-plugin-react](https://github.com/jsx-eslint/eslint-plugin-react) and add `plugin:react/recommended` & `plugin:react/jsx-runtime` to the `extends` list

## Plugins recommended for best dx

- Auto Close Tag
- Auto Import
- Auto Rename Tag
- Better align
- Better Comments
- Code Spell Checker
- Color Info
- Console Ninja
- Debugger for Microsoft Edge
- Dotenv Official + Vault
- Error Lens
- ESLint
- Git Graph
- Git History
- Git Lens
- Image preview
- Import Cost
- Inline fold
- IntelliSense for CSS class names in HTML
- Microsoft Edge Tools for VS Code
- Path Autocomplete
- Path Intellisense
- Prettier
- Pretty Typescript Errors
- Spanish - Code Spell Checker
- Tailwind CSS IntelliSense
- Template String Converter
- Version Lens
- Vite
- Vitest

## Documentation for base libraries

- [Axios](https://axios-http.com/docs/intro)
- [Headless UI](https://headlessui.com)
- [Luxon](https://moment.github.io/luxon/#/?id=luxon)
- [MDN for Js](https://developer.mozilla.org/en-US)
- [React](https://react.dev)
- [React Hook Form](https://react-hook-form.com)
- [React Hot Toast](https://react-hot-toast.com)
- [Tabler Icons](https://tabler-icons.io)
- [Tailwind CSS](https://tailwindcss.com)
- [Tailwind CSS Forms](https://github.com/tailwindlabs/tailwindcss-forms)
- [TanStack Table](https://tanstack.com/table/v8)
- [Typescript](https://www.typescriptlang.org/docs)
- [Valibot (Zod replacement, works like zod with better bundle size, is optional)](https://valibot.dev/guides/introduction/)
- [Vite](https://vitejs.dev/guide)
- [Yarn](https://yarnpkg.com/getting-started)
- [Zod](https://zod.dev/)
