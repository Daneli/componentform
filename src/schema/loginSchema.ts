import { z, ZodType } from 'zod'

type Login = {
	email: string
	password: string
}

export const loginSchema: ZodType<Login> = z.object({
	email: z.string().email('El formato del email no es válido'),
	password: z
		.string()
		.min(6, 'La contraseña debe tener un mínimo de 6 caracteres')
		.max(20, 'La contraseña debe tener un máximo de 20 caracteres')
		.nonempty('La contraseña es requerida'),
})
