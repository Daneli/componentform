import {
	FieldValues,
	SubmitHandler,
	useForm,
	UseFormReturn,
} from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod'
import { z } from 'zod'

type FormProps<TFormValues extends FieldValues> = {
	onSubmit: SubmitHandler<TFormValues>
	children: (methods: UseFormReturn<TFormValues>) => React.ReactNode
	className: string
	validations: z.ZodType
}

export const Form = <
	TFormValues extends Record<string, unknown> = Record<string, unknown>,
>({
	onSubmit,
	children,
	className,
	validations,
}: FormProps<TFormValues>) => {
	const methods = useForm<TFormValues>({ resolver: zodResolver(validations) })
	return (
		<form onSubmit={methods.handleSubmit(onSubmit)} className={className}>
			{children(methods)}
		</form>
	)
}
