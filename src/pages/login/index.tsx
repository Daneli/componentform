import LoginForm from '../../components/loginForm'

function Login() {
	return (
		<div className='max-h-screen-xl flex w-full max-w-screen-xl px-4 sm:px-6 lg:px-8'>
			<div className='flex w-full max-w-lg md:w-screen lg:w-screen xl:w-screen'>
				<LoginForm />
			</div>
		</div>
	)
}

export default Login
